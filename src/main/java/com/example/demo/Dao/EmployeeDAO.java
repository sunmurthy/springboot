package com.example.demo.Dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import com.example.demo.Dto.EmployeeDto;
import com.example.demo.controller.EmployeeController;

@Component
public class EmployeeDAO {

	Logger logger = LoggerFactory.getLogger(EmployeeDAO.class);

	private Map<Integer, EmployeeDto> empMap = new HashMap<Integer, EmployeeDto>();

	public void intialize()
	{
		logger.info("inside -- ini-- before map---");
		
		EmployeeDto dtoOne =  new EmployeeDto(123,"abc");
		EmployeeDto dtoTwo =  new EmployeeDto(124,"def");
		EmployeeDto dtoThree =  new EmployeeDto(125,"thh");
		EmployeeDto dtoFour =  new EmployeeDto(126,"xyz");
		EmployeeDto dtoFive =  new EmployeeDto(127,"pqr");
		
		empMap.put(123, dtoOne);
		empMap.put(124, dtoTwo);
		empMap.put(125,dtoThree);
		empMap.put(126,dtoFour);
		empMap.put(127,dtoFive);
		logger.info("map="+empMap);
	}

	public EmployeeDto getEmployee(int empNo) {
		intialize();
		return empMap.get(empNo);
	}

	public EmployeeDto addEmployee(EmployeeDto emp) {
		empMap.put(emp.getEmpID(), emp);
		return emp;
	}

	public EmployeeDto updateEmployee(EmployeeDto emp) {
		empMap.put(emp.getEmpID(), emp);
		return emp;
	}

	public void deleteEmployee(int empNo) {
		empMap.remove(empNo);
	}

	public List<EmployeeDto> getAllEmployees() {
		logger.info("in dao getall");
		intialize();

		List<EmployeeDto> list = new ArrayList<>();
		list.addAll(empMap.values());
		logger.info("sfh" + list);
		return list;
	}

}
