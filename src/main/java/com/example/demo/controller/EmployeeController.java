package com.example.demo.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.Dao.EmployeeDAO;
import com.example.demo.Dto.EmployeeDto;

@RestController
public class EmployeeController {
	
	Logger logger = LoggerFactory.getLogger(EmployeeController.class);
	@Autowired
	private EmployeeDAO employeeDAO;

	@GetMapping(value="/")
	public String init()
	{
		logger.info("in init() meh");
		employeeDAO.intialize();
		return "welcome to spring boot employee example";
	}
	@GetMapping(value="/empList")
	 public List<EmployeeDto> getEmployees() {
		logger.info("in getEmp");
        return employeeDAO.getAllEmployees();
    }
	
	@GetMapping(value="/emp/{empID}")
	public EmployeeDto getEmpbyID(@PathVariable("empId") int empID )
	{
		
		Map<Integer, EmployeeDto> empMap = new HashMap<Integer, EmployeeDto>();
		
		
			logger.info("inside -- ini-- before map---");
			empMap.put(123, new EmployeeDto(123,"abc"));
			empMap.put(124, new EmployeeDto(124,"def"));
			empMap.put(125, new EmployeeDto(125,"ghi"));
			empMap.put(126, new EmployeeDto(126,"pqr"));
			empMap.put(127, new EmployeeDto(127,"xyz"));
		
		
		
		
		return empMap.get(empID);
	}
	
	@PostMapping(value="/addEmp")
	public EmployeeDto addEmployee(@RequestBody EmployeeDto emp )
	{
		return employeeDAO.addEmployee(emp);
	}
}
