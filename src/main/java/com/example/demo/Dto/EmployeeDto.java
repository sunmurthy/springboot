package com.example.demo.Dto;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import com.example.demo.controller.EmployeeController;



@Component
public class EmployeeDto {

	Logger logger = LoggerFactory.getLogger(EmployeeDto.class);
	
	private int empID;
	
	private String empName;
	
	public EmployeeDto(){
	
	}
	
	public EmployeeDto(int empID,String empName)
	{
		logger.info("in dto");
		this.empID=empID;
		this.empName=empName;
	}

	public int getEmpID() {
		return empID;
	}

	public void setEmpID(int empID) {
		this.empID = empID;
	}

	public String getEmpName() {
		return empName;
	}

	public void setEmpName(String empName) {
		this.empName = empName;
	}

}
